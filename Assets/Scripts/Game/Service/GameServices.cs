﻿using System;
using Core.Event;
using Core.Menu;
using UnityEngine;

namespace Game.Service
{
    public class GameServices : MonoBehaviour, IGameServices
    {
        public static IGameServices Instance
        {
            get;
            private set;
        }
        
        private void Awake()
        {
            // dangerous UnityEngine.Object null check, but this shouldn't really happen.
            if (Instance != null)
            {
                throw new Exception("Services already exists.");
            }
            Instance = this;
            EventManager = new EventManager();
            MenuManager = GetComponentInChildren<MenuManager>();
            DontDestroyOnLoad(gameObject);
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        public IEventManager EventManager { get; private set; }
        public IMenuManager MenuManager { get; private set; }
    }
}