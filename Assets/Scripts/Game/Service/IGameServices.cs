﻿using Core.Event;
using Core.Menu;

namespace Game.Service
{
    public interface IGameServices
    {
        IEventManager EventManager { get; }
        IMenuManager MenuManager { get; }
    }
}