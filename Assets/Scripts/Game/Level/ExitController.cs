﻿using UnityEngine;

namespace Game.Level
{
    public class ExitController : MonoBehaviour
    {
        [SerializeField] public string NextLevel = "";
        [SerializeField] public int SpawnLocation = 0;
    }
}