﻿using System;
using System.Collections;
using System.IO;
using Cinemachine;
using Core.Event;
using Game.Creature;
using Game.Event;
using Game.Service;
using UnityEngine;

namespace Game.Level
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera Camera;
        [SerializeField] private string InitialLevel;
        [SerializeField] private GameObject HeroPrefab;

        private HeroController hero;
        private GameObject levelRoot;
        private string nextLevel;
        private string currentLevel;
        private int spawnID;
        
        private void Start()
        {
            GameServices.Instance.EventManager.Subscribe<HeroDiedEvent>(OnHeroDied);
            GameServices.Instance.EventManager.Subscribe<UseExitEvent>(OnUseExit);

            nextLevel = InitialLevel;
            hero = BuildHero();
            LoadLevel();
        }

        private void LoadLevel()
        {
            StartCoroutine(LoadLevelRoutine());
        }

        private IEnumerator LoadLevelRoutine()
        {
            hero.gameObject.SetActive(false);
            // screen fade
            if (levelRoot != null)
            {
                Destroy(levelRoot);
                yield return new WaitForSeconds(0.1f);
            }
            GameObject levelPrefab = Resources.Load<GameObject>(Path.Combine("Levels", nextLevel));
            currentLevel = nextLevel;
            nextLevel = null;
            levelRoot = Instantiate(levelPrefab);
            hero.gameObject.transform.position = FindSpawnLocation();
            Camera.Follow = hero.transform;
            hero.gameObject.SetActive(true);
            // screen unfade
        }

        private Vector3 FindSpawnLocation()
        {
            foreach (var spawner in levelRoot.GetComponentsInChildren<HeroSpawnLocation>())
            {
                if (spawner.ID == spawnID)
                {
                    return spawner.transform.position;
                }
            }

            throw new Exception("Unable to find spawner for id: " + spawnID);
        }
        
        private void OnUseExit(BaseEvent baseEvent)
        {
            if (baseEvent is UseExitEvent evt)
            {
                Camera.Follow = null;
                nextLevel = evt.Level;
                spawnID = evt.SpawnID;
                LoadLevel();
            }
        }

        private HeroController BuildHero()
        {
            return Instantiate(HeroPrefab).GetComponent<HeroController>();
        }
        
        private void OnHeroDied(BaseEvent baseEvent)
        {
            if (baseEvent is HeroDiedEvent evt)
            {
                Camera.Follow = null;
                nextLevel = InitialLevel;

                hero = BuildHero();
                
                LoadLevel();
            }
        }
    }
}