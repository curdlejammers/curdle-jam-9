﻿using UnityEngine;

namespace Game.Sword
{
    public class SwordController : MonoBehaviour
    {
        [SerializeField] private BoxCollider2D SwordRect;
        void Update()
        {
            Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, SwordRect.size, transform.rotation.z);

            foreach (Collider2D hit in hits)
            {
                if (hit.CompareTag("Enemy"))
                {
                    Destroy(hit.gameObject);
                }
            }
        }
    }
}