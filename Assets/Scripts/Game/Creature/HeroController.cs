﻿using Core.Creature;
using Game.Creature.Configs;
using Game.Event;
using Game.Level;
using Game.Service;
using UnityEngine;

namespace Game.Creature
{
    public class HeroController : CreatureController
    {
        [SerializeField] private Animator Animator;

        private readonly int AttackTrigger = Animator.StringToHash("Attack");
        
        private HeroConfig HeroConfig;
        
        protected override void OnStart(CreatureConfig config)
        {
            HeroConfig = config as HeroConfig;
        }

        protected override void OnUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Attack();
            }
        }

        private void Attack()
        {
            Animator.SetTrigger(AttackTrigger);
        }
        
        protected override void OnEnemyHit(Collider2D hit)
        {
            CurrentHP -= 1;
            if (CurrentHP == 0)
            {
                Die();
            }
            else
            {
                // knockback
                float power = Mathf.Sqrt(2 * HeroConfig.KnockbackPower * Mathf.Abs(Physics2D.gravity.y));
                velocity = new Vector2(hit.transform.position.x < transform.position.x ? power : -power, power);
            }
        }

        protected override void OnUnhandledHit(Collider2D hit)
        {
            if (hit.CompareTag("Door"))
            {
                var evt = new UseExitEvent();
                var exit = hit.GetComponent<ExitController>();
                evt.Level = exit.NextLevel;
                evt.SpawnID = exit.SpawnLocation;
                GameServices.Instance.EventManager.SendEvent(evt);
            }
        }

        protected override void OnDie()
        {
            var evt = new HeroDiedEvent();
            GameServices.Instance.EventManager.SendEvent(evt);
        }
    }
}