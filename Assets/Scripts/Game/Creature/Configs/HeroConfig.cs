﻿using Core.Creature;
using UnityEngine;

namespace Game.Creature.Configs
{
    [CreateAssetMenu]
    public class HeroConfig : CreatureConfig
    {
        public float KnockbackPower = 1;
    }
}