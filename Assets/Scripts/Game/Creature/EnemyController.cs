﻿using Core.Creature;
using Game.Creature.Configs;
using UnityEngine;

namespace Game.Creature
{
    public class EnemyController : CreatureController
    {
        private EnemyConfig EnemyConfig;
        
        protected override void OnStart(CreatureConfig config)
        {
            EnemyConfig = config as EnemyConfig;
        }

        protected override void OnUpdate()
        {
            
        }

        protected override void OnEnemyHit(Collider2D hit)
        {
            Die();
        }

        protected override void OnUnhandledHit(Collider2D hit)
        {
            if (hit.CompareTag("Enemy"))
            {
                return;
            }
            Debug.Log("Collided with " + hit.tag);
        }

        protected override void OnDie()
        {
            
        }
    }
}