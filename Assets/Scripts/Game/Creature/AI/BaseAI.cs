﻿using Core.Creature;
using UnityEngine;

namespace Game.Creature.AI
{
    public abstract class BaseAI : MonoBehaviour
    {
        [SerializeField] protected CreatureController CreatureController;

        void Start()
        {
            OnStart();
        }

        protected abstract void OnStart();
    }
}