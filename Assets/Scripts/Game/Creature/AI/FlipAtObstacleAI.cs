﻿using System;
using System.Collections;
using Core.Creature;
using UnityEngine;

namespace Game.Creature.AI
{
    public class FlipAtObstacleAI : BaseAI
    {
        [SerializeField] private float InitialWaitTime = 0.1f;
        [SerializeField] private float MinWanderBetweenFlips = 1f;
        [SerializeField] private float WaitTimeBeforeFlip = 0.1f;
        
        private float lastFrameXPosition;
        private Coroutine CurrentAction;
        protected override void OnStart()
        {
            lastFrameXPosition = transform.position.x;
            CreatureController.Direction =
                CreatureController.FacingDirection == CreatureController.Facing.Left ? -1 : 1;
            CurrentAction = StartCoroutine(InitialWait());
        }

        void Update()
        {
            float thisFrameX = transform.position.x;
            float distanceThisFrame = Math.Abs(thisFrameX - lastFrameXPosition);
            if (CurrentAction == null && distanceThisFrame < 0.01f)
            {
                CurrentAction = StartCoroutine(TurnAround());
            }

            lastFrameXPosition = thisFrameX;
        }

        IEnumerator InitialWait()
        {
            yield return new WaitForSeconds(InitialWaitTime);
            CurrentAction = null;
        }

        IEnumerator TurnAround()
        {
            yield return new WaitForSeconds(WaitTimeBeforeFlip);
            CreatureController.Direction = -CreatureController.Direction;
            yield return new WaitForSeconds(MinWanderBetweenFlips);
            CurrentAction = null;
        }
    }
}