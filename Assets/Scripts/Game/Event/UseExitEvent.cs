﻿using Core.Event;

namespace Game.Event
{
    public class UseExitEvent : BaseEvent
    {
        public string Level;
        public int SpawnID;
    }
}