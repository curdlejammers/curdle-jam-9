﻿using UnityEngine;

namespace Core.Menu
{
    public abstract class BaseMenu : MonoBehaviour
    {
        public abstract void OnPush(MenuContext context);
    }
}