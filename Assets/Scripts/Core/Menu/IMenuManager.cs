﻿namespace Core.Menu
{
    public interface IMenuManager
    {
        void Push<T>(MenuContext context = null) where T : BaseMenu;
        void Pop<T>() where T : BaseMenu;
        BaseMenu Peek();
    }
}