﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Menu
{
    public class MenuManager : MonoBehaviour, IMenuManager
    {
        [SerializeField] private GameObject[] Prefabs = new GameObject[0];
        
        private Dictionary<Type, GameObject> PrefabRegistry = new Dictionary<Type, GameObject>();

        private Stack<BaseMenu> menus = new Stack<BaseMenu>();

        void Start()
        {
            foreach (GameObject prefab in Prefabs)
            {
                RegisterPrefab(prefab);
            }
        }
        
        public void Push<T>(MenuContext context = null) where T : BaseMenu
        {
            Type type = typeof(T);
            if (!PrefabRegistry.ContainsKey(type))
            {
                throw new ArgumentException("No prefab for that menu type.");
            }
            GameObject newMenu = Instantiate(PrefabRegistry[type], transform);
            var menu = newMenu.GetComponent<BaseMenu>();
            menu.OnPush(context);
            menus.Push(menu);
        }

        public void Pop<T>() where T : BaseMenu
        {
            if (menus.Count == 0)
            {
                throw new ArgumentException("No menus to pop.");
            }
            
            var menu = menus.Pop();

            if (typeof(T) != menu.GetType())
            {
                throw new ArgumentException($"Topmost menu wasn't of type {typeof(T)}");
            }
            
            Destroy(menu.gameObject);
        }

        public BaseMenu Peek()
        {
            if (menus.Count > 0)
            {
                return menus.Peek();                
            }

            return null;
        }

        public void RegisterPrefab(GameObject prefab)
        {
            BaseMenu menu = prefab.GetComponent<BaseMenu>();
            if (menu == null)
            {
                throw new Exception("Prefab did not contain a MenuBase at its root layer.");
            }
            
            PrefabRegistry.Add(menu.GetType(), prefab);
        }
    }
}