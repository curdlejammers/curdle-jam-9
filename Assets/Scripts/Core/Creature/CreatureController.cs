﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Creature
{
    public abstract class CreatureController : MonoBehaviour, ICreatureController
    {
        [SerializeField] private BoxCollider2D Collider;
        [SerializeField] private CreatureConfig Config;
        [SerializeField] private Facing CurrentFacing;

        public bool Grounded { get; private set; } = true;
        public float Direction { get; set; }
        public bool Jump { get; set; }
        public Facing FacingDirection => CurrentFacing;
        
        protected Vector2 velocity;
        
        public int CurrentHP { get; protected set; }
        
        private void Start()
        {
            Flip(CurrentFacing);

            CurrentHP = Config.StartingHeatlh;
            OnStart(Config);
        }

        protected abstract void OnStart(CreatureConfig config);

        private void Update()
        {
            if (Grounded)
            {
                velocity.y = 0;

                if (Jump)
                {
                    // Calculate the velocity required to achieve the target jump height.
                    velocity.y = Mathf.Sqrt(2 * Config.JumpHeight * Mathf.Abs(Physics2D.gravity.y));
                }
            }

            float acceleration = Grounded ? Config.WalkAcceleration : Config.AirAcceleration;
            float deceleration = Grounded ? Config.GroundDeceleration : 0;

            if (Direction != 0)
            {
                if (Direction < 0 && CurrentFacing == Facing.Right)
                {
                    Flip(Facing.Left);
                }
                else if (Direction > 0 && CurrentFacing == Facing.Left)
                {
                    Flip(Facing.Right);
                }
                velocity.x = Mathf.MoveTowards
                    (velocity.x, Config.RunSpeed * Direction, acceleration * Time.deltaTime);
            }
            else
            {
                velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
            }

            velocity.y += Physics2D.gravity.y * Time.deltaTime;

            transform.Translate(velocity * Time.deltaTime);

            Grounded = false;

            // Retrieve all colliders we have intersected after velocity has been applied.
            Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, Collider.size, 0);

            foreach (Collider2D hit in hits)
            {
                // Ignore our own collider.
                if (hit == Collider)
                    continue;

                ColliderDistance2D colliderDistance = hit.Distance(Collider);

                // Ensure that we are still overlapping this collider.
                // The overlap may no longer exist due to another intersected collider
                // pushing us out of this one.
                if (colliderDistance.isOverlapped)
                {
                    if (hit.CompareTag(Config.TakeDamageTag))
                    {
                        OnEnemyHit(hit);
                        continue;
                    }

                    if (hit.CompareTag("KillZone"))
                    {
                        Die();
                        return;
                    }
                    
                    if (!hit.CompareTag("Platform"))
                    {
                        OnUnhandledHit(hit);
                        continue;
                    }

                    Vector3 dist = colliderDistance.pointA - colliderDistance.pointB;
                    transform.Translate(dist);

                    // If we intersect an object beneath us, set grounded to true. 
                    if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
                    {
                        Grounded = true;
                    }
                    
                    // if above, set y velocity to 0.
                    if (Vector2.Angle(colliderDistance.normal, Vector2.down) < 90 && velocity.y > 0)
                    {
                        velocity.y = 0;
                    }
                }
            }
            OnUpdate();
        }
        protected abstract void OnUpdate();
        protected abstract void OnEnemyHit(Collider2D hit);
        protected abstract void OnUnhandledHit(Collider2D hit);
        public void Die()
        {
            var rb = gameObject.AddComponent<Rigidbody2D>();
            rb.AddForce(new Vector2(0, 200));
            rb.AddTorque(500);
            Destroy(GetComponent<BoxCollider2D>());
            Destroy(this);
            Destroy(gameObject, 3f);
            OnDie();
        }

        protected abstract void OnDie();
        
        private void Flip(Facing facing)
        {
            CurrentFacing = facing;
            Vector3 scale = transform.localScale;
            float currentX = Mathf.Abs(scale.x);
            scale.x = facing == Facing.Right ? currentX : -currentX;
            transform.localScale = scale;
        }
        
        public enum Facing
        {
            Right = 0,
            Left
        }
    }
}