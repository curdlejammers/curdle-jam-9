﻿using UnityEngine;

namespace Core.Creature
{
    public abstract class CreatureConfig : ScriptableObject
    {
        public string TakeDamageTag;
        public int StartingHeatlh = 1;
        public float RunSpeed = 7;
        public float JumpHeight = 4;
        public float WalkAcceleration = 75;
        public float AirAcceleration = 30;
        public float GroundDeceleration = 70;
    }
}