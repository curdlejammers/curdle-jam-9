﻿namespace Core.Creature
{
    public interface ICreatureController
    {
        float Direction { get; set; }
        bool Jump { get; set; }
    }
}