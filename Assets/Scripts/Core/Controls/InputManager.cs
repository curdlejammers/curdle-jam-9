﻿using Core.Creature;
using UnityEngine;

namespace Core.Controls
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField] private CreatureController CreatureController;

        void Update()
        {
            CreatureController.Jump = Input.GetKey(KeyCode.Space);

            CreatureController.Direction = Input.GetAxis("Horizontal");
        }
    }
}