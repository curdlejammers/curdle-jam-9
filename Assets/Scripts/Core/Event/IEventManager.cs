﻿using System;

namespace Core.Event
{
    public interface IEventManager
    {
        void Subscribe<T>(Action<BaseEvent> listener) where T : BaseEvent;
        void Unsubscribe<T>(Action<BaseEvent> listener) where T : BaseEvent;
        void SendEvent<T>(T evt) where T : BaseEvent;
    }
}