﻿using System;
using System.Collections.Generic;

namespace Core.Event
{
    public class EventManager : IEventManager
    {
        private Dictionary<Type, HashSet<Action<BaseEvent>>> listeners = 
            new Dictionary<Type, HashSet<Action<BaseEvent>>>();
        
        public void Subscribe<T>(Action<BaseEvent> listener) where T : BaseEvent
        {
            if (listener == null)
            {
                throw new ArgumentException("listener cannot be null.");
            }
            
            Type type = typeof(T);
            if (!listeners.ContainsKey(type))
            {
                listeners.Add(type, new HashSet<Action<BaseEvent>>()
                {
                    listener
                });
            }
            else
            {
                var typeListeners = listeners[type];
                if (typeListeners.Contains(listener))
                {
                    throw new ArgumentException("This listener has already subscribed to this event type.");
                }
                typeListeners.Add(listener);
            }
        }

        public void Unsubscribe<T>(Action<BaseEvent> listener) where T : BaseEvent
        {
            Type type = typeof(T);
            bool found = false;
            if (listeners.ContainsKey(type))
            {
                found = listeners[type].Remove(listener);
            }

            if (!found)
            {
                throw new ArgumentException("That listener was not subscribed to that event.");
            }
        }

        public void SendEvent<T>(T evt) where T : BaseEvent
        {
            Type type = typeof(T);
            if (!listeners.ContainsKey(type))
            {
                return;
            }
            
            foreach (var listener in listeners[type])
            {
                listener(evt);
            }
        }
    }
}