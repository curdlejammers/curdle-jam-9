﻿using System;
using Core.Event;
using NUnit.Framework;

namespace Tests.EditMode.Core.Event
{
    public class EventManagerTests
    {
        private IEventManager eventManager;

        [SetUp]
        public void SetUp()
        {
            eventManager = new EventManager();
        }

        [Test]
        public void Subscribe()
        {
            eventManager.Subscribe<TestEvent1>(evt => { });
        }

        [Test]
        public void Send()
        {
            TestEvent1 testEvent1 = new TestEvent1();
            bool called = false;

            Action<BaseEvent> listener = evt =>
            {
                called = true;
                Assert.AreEqual(testEvent1, evt);
            };
            
            eventManager.Subscribe<TestEvent1>(listener);
            eventManager.SendEvent(testEvent1);

            Assert.AreEqual(true, called);
        }

        [Test]
        public void DuplicateSubscription()
        {
            TestEvent1 testEvent1 = new TestEvent1();
            bool called = false;

            Action<BaseEvent> listener = evt =>
            {
                called = true;
                Assert.AreEqual(testEvent1, evt);
            };
            
            eventManager.Subscribe<TestEvent1>(listener);
            Assert.Throws<ArgumentException>(() =>
            {
                eventManager.Subscribe<TestEvent1>(listener);
            });
        }
        
        [Test]
        public void UnsubscribeWithoutSubscribe()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                eventManager.Unsubscribe<TestEvent1>(evt =>
                {
                    
                });
            });
        }

        [Test]
        public void NullSubscription()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                eventManager.Subscribe<TestEvent1>(null);
            });
        }
        
        [Test]
        public void MultipleSubscribers()
        {
            TestEvent1 testEvent1 = new TestEvent1();
            bool called1 = false;
            bool called2 = false;

            Action<BaseEvent> listener1 = evt =>
            {
                called1 = true;
                Assert.AreEqual(testEvent1, evt);
            };
            
            Action<BaseEvent> listener2 = evt =>
            {
                called2 = true;
                Assert.AreEqual(testEvent1, evt);
            };
            
            eventManager.Subscribe<TestEvent1>(listener1);
            eventManager.Subscribe<TestEvent1>(listener2);
            eventManager.SendEvent(testEvent1);

            Assert.AreEqual(true, called1);
            Assert.AreEqual(true, called2);
        }
        
        
        [Test]
        public void OnlySendCorrectEventType()
        {
            TestEvent1 testEvent1 = new TestEvent1();
            bool called1 = false;
            bool called2 = false;

            Action<BaseEvent> listener1 = evt =>
            {
                called1 = true;
            };
            
            Action<BaseEvent> listener2 = evt =>
            {
                called2 = true;
            };
            
            eventManager.Subscribe<TestEvent1>(listener1);
            eventManager.Subscribe<TestEvent2>(listener2);
            eventManager.SendEvent(testEvent1);

            Assert.AreEqual(true, called1);
            Assert.AreEqual(false, called2);
        }
        
        [Test]
        public void Unsubscribe()
        {
            TestEvent1 testEvent1 = new TestEvent1();
            bool called = false;
            Action<BaseEvent> listener = evt =>
            {
                called = true;
            };
            
            eventManager.Subscribe<TestEvent1>(listener);
            eventManager.Unsubscribe<TestEvent1>(listener);
            eventManager.SendEvent(testEvent1);

            Assert.AreEqual(false, called);
        }

        [Test]
        public void NoSubscribers()
        {
            eventManager.SendEvent(new TestEvent1());
        }
        
        [Test]
        public void NullEvent()
        {
            bool called = false;

            Action<BaseEvent> listener = evt =>
            {
                called = true;
                Assert.IsNull(evt);
            };
            
            eventManager.Subscribe<TestEvent1>(listener);
            eventManager.SendEvent<TestEvent1>(null);

            Assert.AreEqual(true, called);
        }

        private class TestEvent1 : BaseEvent
        {
            
        }
        
        private class TestEvent2 : BaseEvent
        {
            
        }
    }
}