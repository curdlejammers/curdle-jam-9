﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Menu;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace Tests.PlayMode.Core.Menu
{
    public class MenuManagerPrefab
    {
        private readonly string PrefabPath = "Assets/Menus/MenuManager.prefab";
        private IMenuManager menuManager;
        
        [UnitySetUp]
        public void UnitySetUp()
        {
            var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(PrefabPath);
            GameObject go = Object.Instantiate(prefab);
            menuManager = go.GetComponent<MenuManager>();
        }

        [UnityTest]
        public IEnumerator IsPrefabRegistered([ValueSource(nameof(MenuBases))] Type type)
        {
            if (menuManager == null)
            {
                UnitySetUp();
            }
            
            yield return new WaitForEndOfFrame();
            
            try
            {
                MethodInfo method = typeof(MenuManager).GetMethod(nameof(MenuManager.Push));
                MethodInfo typedMethod = method.MakeGenericMethod(type);
                typedMethod.Invoke(menuManager, new object[]
                {
                    null
                });
            }
            catch (Exception e)
            {
                Assert.Fail($"Exception trying to push type {type}: {e}");
            }
        }

        private static IEnumerable<Type> MenuBases()
        {
            var gameAssembly =     AppDomain.CurrentDomain.GetAssemblies().
                SingleOrDefault(assembly => assembly.GetName().Name == "Game");
            
            var types = new List<Type>();
            foreach (Type type in 
                gameAssembly.GetTypes()
                    .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(BaseMenu))))
            {
                types.Add(type);
            }
            return types;
        }
    }
}