﻿using System;
using System.Collections;
using Core.Menu;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace Tests.PlayMode.Core.Menu
{
    public class MenuManagerTests
    {
        private IMenuManager menuManager;
        
        [SetUp]
        public void SetUp()
        {
            GameObject go = new GameObject();
            MenuManager menuManager = go.AddComponent<MenuManager>();
            menuManager.RegisterPrefab(CreatePrefab<TestMenu1>());
            menuManager.RegisterPrefab(CreatePrefab<TestMenu2>());
            this.menuManager = menuManager;
        }

        private GameObject CreatePrefab<T>() where T : BaseMenu
        {
            GameObject go = new GameObject();
            go.AddComponent<T>();
            return go;
        }
        
        [Test]
        public void Push()
        {
            menuManager.Push<TestMenu1>();
            Assert.AreEqual(true, IsMenuShowing<TestMenu1>());
        }
        
        [Test]
        public void PushWithContext()
        {
            MenuContext context = new TestMenu1Context();
            menuManager.Push<TestMenu1>(context);
            
            Assert.AreEqual(context, ((TestMenu1)menuManager.Peek()).Context);
        }

        [Test]
        public void Peek()
        {
            Assert.IsNull(menuManager.Peek());
            menuManager.Push<TestMenu1>();
            Assert.IsNotNull(menuManager.Peek());
        }
        
        [UnityTest]
        public IEnumerator Pop()
        {
            menuManager.Push<TestMenu1>();
            menuManager.Pop<TestMenu1>();
            Assert.IsNull(menuManager.Peek());
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(false, IsMenuShowing<TestMenu1>());
        }

        [Test]
        public void PopMultipleTypes()
        {
            menuManager.Push<TestMenu1>();
            menuManager.Push<TestMenu2>();
            menuManager.Pop<TestMenu2>();
            Assert.AreEqual(typeof(TestMenu1), menuManager.Peek().GetType());
            menuManager.Pop<TestMenu1>();
            Assert.IsNull(menuManager.Peek());
        }
        
        [Test]
        public void GenericPop()
        {
            menuManager.Push<TestMenu1>();
            menuManager.Pop<TestMenu1>();
        }
        
        [Test]
        public void PopWithNoneOpen()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                menuManager.Pop<TestMenu1>();
            });
        }

        [Test]
        public void PopWrongMenu()
        {
            menuManager.Push<TestMenu2>();
            
            Assert.Throws<ArgumentException>(() =>
            {
                menuManager.Pop<TestMenu1>();
            });
        }
        
        private bool IsMenuShowing<T>() where T : BaseMenu
        {
            var menuManagerComponent = (MenuManager) menuManager;
            return menuManagerComponent.GetComponentsInChildren<T>().Length > 0;
        }
        
        private class TestMenu1Context : MenuContext
        {
            
        }
        
        private class TestMenu1 : BaseMenu
        {
            public MenuContext Context;

            public override void OnPush(MenuContext context)
            {
                Context = context;
            }
        }

        private class TestMenu2 : BaseMenu
        {
            public override void OnPush(MenuContext context)
            {
            }
        }
    }
}