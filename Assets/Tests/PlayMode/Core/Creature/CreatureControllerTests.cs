﻿using System.Collections;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace Core.Creature
{
    public class CreatureControllerTests
    {
        private readonly string prefabPath = "Assets/Tests/PlayMode/Core/Creature/CreatureControllerTestsPrefab.prefab";
        private GameObject go;
        private ICreatureController creatureController;

        [UnitySetUp]
        public IEnumerator SetUp()
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Object.Instantiate(prefab);
            creatureController = Object.FindObjectOfType<CreatureController>();
            go = ((CreatureController) creatureController).gameObject;
            yield return null;
        }

        [UnityTest]
        public IEnumerator StandStill()
        {
            float prevMag = go.transform.position.magnitude;
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForEndOfFrame();
                float newMag = go.transform.position.magnitude;
                Assert.AreEqual(prevMag, newMag);
            }
        }

        [UnityTest]
        public IEnumerator RunRight()
        {
            float prevLoc = go.transform.position.x;
            creatureController.Direction = 1;
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForEndOfFrame();
                float newLoc = go.transform.position.x;
                Assert.Greater(newLoc, prevLoc);
            }
        }

        [UnityTest]
        public IEnumerator RunLeft()
        {
            float prevLoc = go.transform.position.x;
            creatureController.Direction = -1;
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForEndOfFrame();
                float newLoc = go.transform.position.x;
                Assert.Less(newLoc, prevLoc);
            }
        }

        [UnityTest]
        public IEnumerator Jump()
        {
            float prevLoc = go.transform.position.y;
            creatureController.Jump = true;
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForEndOfFrame();
                float newLoc = go.transform.position.y;
                Assert.Greater(newLoc, prevLoc);
            }
        }

        [UnityTest]
        public IEnumerator LandAfterJump()
        {
            float prevLoc = go.transform.position.y;
            creatureController.Jump = true;

            yield return new WaitForSeconds(3);

            Assert.AreEqual(prevLoc, go.transform.position.y);
        }

        [UnityTest]
        public IEnumerator CantWalkThroughWalls()
        {
            var wall = GameObject.Find("Wall");
            wall.SetActive(true);

            creatureController.Direction = 1;

            yield return new WaitForSeconds(2);
            
            Assert.Less(go.transform.position.x, wall.transform.position.x);
        }
    }
}